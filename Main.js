/*
    TODO

*/
class Main {
    constructor() {
        this.app = new PIXI.Application({ background: '#444444', width: WIDTH, height: HEIGHT });
        document.body.appendChild(this.app.view);

        this.menu = null;
        this.game = null;
        this.status = 1;
    
        // Listen for animate update
        this.app.ticker.add((delta) => {
            this.update(delta);
        });        
    }

    update(delta) {
        switch(this.status) {
            case 0:
                if(this.menu == null) {
                    this.clearAll();
                    this.menu = new Menu(this.app);
                }

                this.status = this.menu.update(delta);    
                
                break;
            case 1:
                if(this.game == null) {
                    this.clearAll();
                    this.game = new Game(this.app);
                }
                this.status = this.game.update(delta);
                break;
        }
    }

    clearAll() {
        if(this.menu != null) {
            this.menu.clear();
            this.menu = null;
        }
        
        if(this.game != null) {
            this.game.clear();
            this.game = null;
        }
    }
}

const WIDTH = 800;
const HEIGHT = 600;
const GRAVITY = 9.8;

let main = new Main();