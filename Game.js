let CAR = null;
let ROAD = null;
class Game {
	constructor(app) {
		this.app = app

		this.background = new Paralax();
		this.app.stage.addChild(this.background);
		this.x = 1;

		this.car = new Car();

		CAR = this.car;

		this.app.stage.addChild(this.car);
		
		this.road = new Road();
		ROAD = this.road;
		this.app.stage.addChild(this.road);

		this.hasGravity = false;

	}

	update(delta) {
		this.x += 1;
		this.car.update(this.hasGravity);
		this.background.update(this.x, 0); //this.car.x, this.car.y);

		if(this.car.onRoadCollision(this.road)) {
			console.log("<-COLLISION->");
			this.hasGravity = false;
		}

		return 1;
	}

	clear() {
		
	}
}