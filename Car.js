class Car extends PIXI.Container {
    constructor() {
        super();

        const tireTex = PIXI.Texture.from('tire.png');
        
        this.tire1 = new PIXI.Sprite(tireTex);
		this.tire1.x = 385;
		this.tire1.y = 220;
        this.tire1.anchor.set(0.5);
		this.addChild(this.tire1);
        
        this.tire2 = new PIXI.Sprite(tireTex);
		this.tire2.x = 115;
		this.tire2.y = 220;
        this.tire2.anchor.set(0.5);
		this.addChild(this.tire2);

        this.body = new PIXI.Sprite(PIXI.Texture.from('car.png'));
		this.body.x = 265;
		this.body.y = 140;
        this.body.anchor.set(0.5);
		this.addChild(this.body);

        this.scale.set(0.33, 0.33);

        this.keyAcc = 'p';
        this.keyBreak = 'q';

        this.friction = 0.98;
        this.break = false;
        this.acc = false;

        this.movX = 0;
        this.lastMovX = 0;
        this.deltaMovX = 0;

        this.movY = 0;
        this.weight = 0.001;

        this.testCollisionPoints = [
            { x: 385, y: 220 },
            { x: 115, y: 220 },
            { x: 520, y: 220 },
            { x: 520, y: 100 },
            { x: 40, y: 40 },
            { x: 50, y: 50 },
            { x: 60, y: 60 },
            { x: 70, y: 70 },
        ];

        this.graphics = new PIXI.Graphics();
        for(let point of this.testCollisionPoints) {
            this.makeCircle(point);
        }
        this.graphics.endFill();
        this.addChild(this.graphics);

        this.y = 100;

        window.onkeydown = (evt) => {
            if (evt.key == this.keyAcc) {
                this.acc = true;
            }
            if (evt.key == this.keyBreak) {
                this.break = true;
            }
        };

        window.onkeyup = (evt) => {
            if (evt.key == this.keyAcc) {
                this.acc = false;
            }
            if (evt.key == this.keyBreak) {
                this.break = false;
            }
        };
    }

    onRoadCollision(road) {
        for(let point of this.testCollisionPoints) {
            if(road.hitArea.contains(this.x + (point.x * 0.33), this.y + (point.y * 0.33))) {
                return true;
            }
            return false;
        }

        return false;
    }

    update(hasGravity) {
        // X
        if (this.acc) {
            this.movX += 0.2;
        }

        if (this.break) {
            this.friction = 0.85;
        } else {
            this.friction = 0.98;
        }

        this.movX *= this.friction;
        this.x += this.movX;

        this.tire1.rotation += this.movX * 0.05;
        this.tire2.rotation += this.movX * 0.05;

        if(this.lastMovX != 0) {
            this.deltaMovX = this.lastMovX - this.movX;
            
            this.body.rotation += this.deltaMovX * 0.02;
        }

        this.lastMovX = this.movX;

        if(hasGravity) {
            // y
            this.movY += this.weight * GRAVITY;
            this.y += this.movY;
        }
    }

    makeCircle({x, y}) {
        this.graphics.lineStyle(0); // draw a circle, set the lineStyle to zero so the circle doesn't have an outline
        this.graphics.beginFill(0xDE3249, 1);
        this.graphics.drawCircle(x, y, 10);
        this.graphics.endFill();
    }
}