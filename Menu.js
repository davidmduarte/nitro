class Menu {
	constructor(app) {
		this.app = app;

		this.goto = 0;

		this.bg = new PIXI.Sprite(PIXI.Texture.from('bg.jpeg'));
		this.bg.x = 0;
		this.bg.y = 0;
		this.app.stage.addChild(this.bg);

		this.layer = new PIXI.Graphics();

		this.layer.beginFill(0x000000);
		this.layer.drawRect(0, 0, WIDTH, HEIGHT);
		this.layer.endFill();

		this.app.stage.addChild(this.layer);		

		this.style = new PIXI.TextStyle({
			fontFamily: 'Arial',
			fontSize: 36,
			fontStyle: 'italic',
			fontWeight: 'bold',
			fill: ['#ffffff', '#00ff99'],
			stroke: '#4a1850',
			strokeThickness: 5,
			dropShadow: true,
			dropShadowColor: '#000000',
			dropShadowBlur: 4,
			dropShadowAngle: Math.PI / 6,
			dropShadowDistance: 6,
			wordWrap: true,
			wordWrapWidth: 440,
			lineJoin: 'round',
		});

		this.style2 = new PIXI.TextStyle({
			fontFamily: 'Arial',
			fontSize: 30,
			fontStyle: 'italic',
			fontWeight: 'bold',
			fill: ['#ffffff', '#99ff99'],
			stroke: '#4a1850',
			strokeThickness: 1,
			wordWrap: true,
			wordWrapWidth: 440,
			lineJoin: 'round',
		});

		this.style3 = new PIXI.TextStyle({
			fontFamily: 'Arial',
			fontSize: 30,
			fontStyle: 'italic',
			fontWeight: 'bold',
			fill: ['#ffffff', '#99ff99'],
			stroke: '#4a1850',
			strokeThickness: 1,
			wordWrap: true,
			wordWrapWidth: 440,
			lineJoin: 'round',
		});

		this.title = new PIXI.Text('NITRO', this.style);
		this.title.x = 50;
		this.title.y = HEIGHT / 4;

		this.start = new PIXI.Text('START', this.style2);
		this.start.x = 50;
		this.start.oriX = 50;
		this.start.y = HEIGHT / 2 - 50;
		this.start.isPointerOver = false;
		this.start.cnt = 0;

		this.start.interactive = true;
		this.start.cursor = 'pointer';

		this.start.on('pointerdown', () => {
			console.log("pointerdown");
			this.goto = 1;
		});
		this.start.on('pointerover', () => {
			this.start.isPointerOver = true;
			this.start.style.fill = ['#000000', '#999999'];
		})
        this.start.on('pointerout', () => {
			this.start.isPointerOver = false;
			this.start.style.fill = ['#ffffff', '#99ff99'];
		});
		
		this.rankings = new PIXI.Text('RANKINGS', this.style3);
		this.rankings.x = 50;
		this.rankings.oriX = 50;
		this.rankings.y = HEIGHT / 2;
		this.rankings.isPointerOver = false;
		this.rankings.cnt = 0;

		this.rankings.interactive = true;
		this.rankings.cursor = 'pointer';

		this.rankings.on('pointerdown', function() {
			console.log("pointerdown");
		});
		this.rankings.on('pointerover', () => {
			this.rankings.isPointerOver = true;
			this.rankings.style.fill = ['#000000', '#999999'];
		})
        this.rankings.on('pointerout', () => {
        	this.rankings.isPointerOver = false;
			this.rankings.style.fill = ['#ffffff', '#99ff99'];
		});
	
		this.app.stage.addChild(this.title);
		this.app.stage.addChild(this.start);
		this.app.stage.addChild(this.rankings);
	}

	update(delta) {
		if(this.start.isPointerOver) this.start.cnt += 0.3;
		else this.start.cnt = 0;

		this.start.x = this.start.oriX + Math.sin(this.start.cnt) * 5;

		if(this.rankings.isPointerOver) this.rankings.cnt += 0.3;
		else this.rankings.cnt = 0;

		this.rankings.x = this.rankings.oriX + Math.sin(this.rankings.cnt) * 5;

		if(this.layer.alpha > 0.35) this.layer.alpha -= 0.007;
		console.log("goto:", this.goto);
		return this.goto;
	}

	clear() {
		this.app.stage.removeChild(this.bg);
		this.app.stage.removeChild(this.layer);
		this.app.stage.removeChild(this.title);
		this.app.stage.removeChild(this.start);
		this.app.stage.removeChild(this.rankings);	
	}
}