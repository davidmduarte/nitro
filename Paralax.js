class Paralax extends PIXI.Container {
	constructor() {
		super();

		this.bg = new PIXI.Sprite(PIXI.Texture.from('bg.jpeg'));
		this.bg.x = 0;
		this.bg.y = -100;
		this.addChild(this.bg);

		this.bg1 = new PIXI.Sprite(PIXI.Texture.from('bg.jpeg'));
		this.bg1.x = this.bg.width;
		this.bg1.y = 0;
		this.addChild(this.bg1);

		this.bg2 = new PIXI.Sprite(PIXI.Texture.from('bg.jpeg'));
		this.bg2.x = this.bg.width * 2;
		this.bg2.y = 0;
		this.addChild(this.bg2);
	}

	update(x, y) {
		x %= this.bg.width;
		x *= -1;
		this.bg.x = x;
		this.bg1.x = x + this.bg.width;
		this.bg2.x = x + (this.bg.width * 2);

		if(this.bg.x < -this.bg.width) {
			this.bg.x = Math.max(this.bg1.x, this.bg2.x) + this.bg.width;
		}

		if(this.bg1.x < -this.bg.width) {
			this.bg1.x = Math.max(this.bg.x, this.bg2.x) + this.bg.width;
		}

		if(this.bg2.x < -this.bg.width) {
			this.bg2.x = Math.max(this.bg.x, this.bg1.x) + this.bg.width;
		}
	}
}