class Road extends PIXI.Container {
    constructor() {
        super();

        this.road = new PIXI.Graphics();

        const path = [
            0, 550, 
            50, 570, 
            100, 550, 
            150, 570, 
            200, 550, 
            250, 570, 
            300, 550, 
            350, 570, 
            400, 550, 
            450, 570, 
            500, 550,
            550, 570,
            600, 550,
            650, 570,
            700, 550,
            750, 570,
            800, 520, 
            
            800, 600, 
            0  , 600
        ];

        this.road.lineStyle(0);
        this.road.beginFill(0x3500FA, 1);
        this.road.drawPolygon(path);
        this.road.endFill();

        this.interactive = true;
        this.hitArea = new PIXI.Polygon(path);

        this.addChild(this.road);

    }

    update() {
       

    }
}